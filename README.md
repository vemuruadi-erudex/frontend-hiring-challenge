# Erudex Hiring Challenge
Coding assignment for hiring Erudex frontend developer.

## How to go about solving the problem
https://apdcr.erudex.com/reports/district-wise-usage-report.php

This assignment is about building Andhra Pradesh Govt Schools Digital Class Room (DCR) usage reports User Interface using React stack.
Explore the above URL to understand the target app interface.
- Use Next.JS/CRA with styled components and any css library like Material, bootstrap or event Tailwind
- Use recharts kind of visualization library for graphs
- Use JSON below for data

## Expected outcomes
- Working demo hosted on netlify
- Github code repo link
- Proper JS coding guidelines, preferably use ES6
- Component planning is key thing (Using design system approach is best thing)
- Automated unit testing using Jest and Enzyme

## Any questions?
Use https://bitbucket.org/vemuruadi-erudex/frontend-hiring-challenge/issues for any doubts/clarifications during the assignment



## Sample Data
### DCR Usage - District Wise Report in Hours

Sample JSON data
```
{
  "result": [
    {
      "district": "WEST GODAVARI",
      "seconds": "2683734"
    },
    {
      "district": "EAST GODAVARI",
      "seconds": "1203432"
    },
    {
      "district": "KRISHNA",
      "seconds": "934886"
    },
    {
      "district": "GUNTUR",
      "seconds": "622611"
    },
    {
      "district": "ANANTAPUR",
      "seconds": "540785"
    },
    {
      "district": "CHITTOOR",
      "seconds": "518314"
    },
    {
      "district": "VISAKHAPATNAM",
      "seconds": "354883"
    },
    {
      "district": "VIZIANAGARAM",
      "seconds": "324639"
    },
    {
      "district": "SRIKAKULAM",
      "seconds": "212238"
    },
    {
      "district": "PRAKASAM",
      "seconds": "87489"
    },
    {
      "district": "SPSR NELLORE",
      "seconds": "41224"
    },
    {
      "district": "KADAPA",
      "seconds": "18252"
    },
    {
      "district": "KURNOOL",
      "seconds": "13076"
    },
    {
      "district": "NELLORE",
      "seconds": "2440"
    }
  ]
}
```

### Total Hours Usage - No. Of Schools

Sample JSON Data
```
{
  "result": [
    {
      "range": "Over 5",
      "count": "117"
    },
    {
      "range": "4 To 5",
      "count": "50"
    },
    {
      "range": "3 To 4",
      "count": "85"
    },
    {
      "range": "2 To 3",
      "count": "109"
    },
    {
      "range": "1 To 2",
      "count": "180"
    },
    {
      "range": "0 To 1",
      "count": "546"
    },
    {
      "range": "0",
      "count": "2779"
    }
  ]
}
```

### Total Days Usage - No. Of Schools

Sample JSON Data
```
{
  "result": [
    {
      "range": "Over 20",
      "count": "0"
    },
    {
      "range": "15 To 20",
      "count": "0"
    },
    {
      "range": "10 To 15",
      "count": "0"
    },
    {
      "range": "5 To 10",
      "count": "0"
    },
    {
      "range": "1 To 5",
      "count": "1087"
    },
    {
      "range": "0",
      "count": "2779"
    }
  ]
}
```

### DCR Usage - District Wise Report

Sample JSON Data
```
{
  "result": [
    {
      "district": "ANANTAPUR",
      "months": "[\"Sep-2019\"]",
      "hours": "[540785]",
      "schools_count": "[108]"
    },
    {
      "district": "CHITTOOR",
      "months": "[\"Sep-2019\"]",
      "hours": "[518314]",
      "schools_count": "[89]"
    },
    {
      "district": "EAST GODAVARI",
      "months": "[\"Sep-2019\"]",
      "hours": "[1203432]",
      "schools_count": "[116]"
    },
    {
      "district": "GUNTUR",
      "months": "[\"Sep-2019\"]",
      "hours": "[622611]",
      "schools_count": "[113]"
    },
    {
      "district": "KADAPA",
      "months": "[\"Sep-2019\"]",
      "hours": "[18252]",
      "schools_count": "[7]"
    },
    {
      "district": "KRISHNA",
      "months": "[\"Sep-2019\"]",
      "hours": "[934886]",
      "schools_count": "[97]"
    },
    {
      "district": "KURNOOL",
      "months": "[\"Sep-2019\"]",
      "hours": "[13076]",
      "schools_count": "[11]"
    },
    {
      "district": "NELLORE",
      "months": "[\"Sep-2019\"]",
      "hours": "[2440]",
      "schools_count": "[2]"
    },
    {
      "district": "PRAKASAM",
      "months": "[\"Sep-2019\"]",
      "hours": "[87489]",
      "schools_count": "[32]"
    },
    {
      "district": "SPSR NELLORE",
      "months": "[\"Sep-2019\"]",
      "hours": "[41224]",
      "schools_count": "[12]"
    },
    {
      "district": "SRIKAKULAM",
      "months": "[\"Sep-2019\"]",
      "hours": "[212238]",
      "schools_count": "[61]"
    },
    {
      "district": "VISAKHAPATNAM",
      "months": "[\"Sep-2019\"]",
      "hours": "[354883]",
      "schools_count": "[87]"
    },
    {
      "district": "VIZIANAGARAM",
      "months": "[\"Sep-2019\"]",
      "hours": "[324639]",
      "schools_count": "[74]"
    },
    {
      "district": "WEST GODAVARI",
      "months": "[\"Sep-2019\"]",
      "hours": "[2683734]",
      "schools_count": "[278]"
    }
  ]
}
```
